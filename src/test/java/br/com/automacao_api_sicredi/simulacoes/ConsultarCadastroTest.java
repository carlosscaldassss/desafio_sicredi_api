package br.com.automacao_api_sicredi.simulacoes;

import br.com.automacao_api_sicredi.baseteste.BaseTeste;
import br.com.automacao_api_sicredi.dto.MensagemDTO;
import br.com.automacao_api_sicredi.dto.OutSimulacaoDTO;
import br.com.automacao_api_sicredi.service.SimulacaoService;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.Test;
import org.testng.Assert;


@Feature("Testes de Consulta de Cadastros")
public class ConsultarCadastroTest extends BaseTeste{
    SimulacaoService simulacaoService = new SimulacaoService();

    @Test
    @Description("Espera que retorne todas as simulações cadastradas: ")
    public void realizarConsultaDeTodasAsSimulacoesCadastradas(){
        OutSimulacaoDTO[] resultFromREST = SimulacaoService.consultarTodasSimulacoes(200);
        System.out.println(resultFromREST);
    }

    @Test
    @Description("Espera que retorne um resultado com os dados do cadastro")
    public void realizarConsultaPorCPFQuePossuiCadastroSimulacao() {
        String cpf = "17822386034";
       OutSimulacaoDTO resultFromREST = SimulacaoService.consultarSimulacaoComCPF(cpf, 200);

       System.out.println(resultFromREST.getSeguro());

        Assert.assertEquals(resultFromREST.getNome(), "Deltrano");
        Assert.assertEquals(resultFromREST.getCpf(), "17822386034");
        Assert.assertEquals(resultFromREST.getEmail(),"deltrano@gmail.com");
        Assert.assertEquals(resultFromREST.getValor(),"20000"); //estaVindoCom.0 no final
        Assert.assertEquals(resultFromREST.getParcelas(), 5);
        Assert.assertEquals(resultFromREST.getSeguro(), "false");

    }

    @Test
    @Description("Espera que retorne uma mensagem de erro informando que o CPF não está no cadastro")
    public void realizarConsultaPorCPFQueNaoPossuiCadastroNaSimulacao() {
        String cpf = "00000000011";

        MensagemDTO erroFromRest = SimulacaoService.consultarSimulacaoCpfNaoCadastrado(cpf, 404);
        Assert.assertEquals(erroFromRest.getMensagem(), "CPF "+cpf+" não encontrado");
    }
}
