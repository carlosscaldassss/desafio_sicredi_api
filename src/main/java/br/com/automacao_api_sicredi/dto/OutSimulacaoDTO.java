package br.com.automacao_api_sicredi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class OutSimulacaoDTO {
    private int id;
    private String nome;
    private String cpf;
    private String email;
    private float valor;
    private int parcelas;
    private String seguro;
}