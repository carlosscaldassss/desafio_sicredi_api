package br.com.automacao_api_sicredi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MensagemDTO {
    private Object erros;
    private String mensagem;
}
