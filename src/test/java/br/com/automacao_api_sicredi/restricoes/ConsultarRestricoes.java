package br.com.automacao_api_sicredi.restricoes;

import br.com.automacao_api_sicredi.baseteste.BaseTeste;
import br.com.automacao_api_sicredi.dto.MensagemDTO;
import br.com.automacao_api_sicredi.service.RestricaoService;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.Assert;
import org.junit.Test;
import java.util.Arrays;
import java.util.List;
import java.util.Random;



@Feature("Testes de Consulta de Registros")
public class ConsultarRestricoes extends BaseTeste {

    @Test
    @Description("TESTE")
    public void consultarUmCpfComRestricao(){
        List<String> cpfs = Arrays.asList("97093236014", "60094146012", "84809766080", "62648716050", "26276298085", "01317496094", "55856777050", "19626829001","24094592008", "58063164083");
        String cpf = cpfs.get(new Random().nextInt(cpfs.size()));
        MensagemDTO erroFromREST = RestricaoService.consultarCpfComRestricao(cpf, 200);
        Assert.assertEquals("O CPF "+cpf+" tem problema", erroFromREST.getMensagem());
    };
    @Test
    @Description("TESTE 2")
    public void consultarUmCpfSemRestricao(){
        String cpf = "97093236015";
        RestricaoService.consultarCpfSemRestricao(cpf, 204);
    };
}
