# Desafio Automação API SICREDI

## Requisitos

* Java 8+ JDK
* Apache Maven
* Subir a aplicação back-end

**Observação:** depois que fizer o download do Apache Maven, não se esqueça de adicioná-lo como uma variável de ambiente no seu sistema operacional.

## Ferramentas e bibliotecas
* JUnit
* <a href="https://rest-assured.io/" target="_blank">Rest Assured</a>
* <a href="http://allure.qatools.ru/" target="_blank">Allure reports</a>

## Como executar aplicação back-end
- A aplicação back-end esta raiz do projeto com o nome 'Provas Sicredi-CAS.zip'. Extraia o arquivo no local de sua preferência.
- Na raiz do projeto 'Provas Sicredi_CAS', através de seu Prompt de Commando/Terminal/Console execute o comando:
```bash
  mvn clean spring-boot:run
 ```
 - A aplicação estará disponível através da URL http://localhost:8080
 - Você pode trocar a porta da aplicação, caso a 8080 já estiver em uso, adicionando a propriedade JVM server.port .
  ```bash
  mvn clean spring-boot:run -Dserver.port=8888
 ```
 - OBS: Caso altere a porta padrão, será necessário alterar a porta na aplicação de testes na class BaseTeste.

### Comandos para executar os testes

Na raiz do projeto dos testes execute os seguintes comandos:

* Somente testes 
```bash
  mvn clean test
 ```
  
  
* Executar o relatório localmente no seu browser
```bash
  mvn allure:serve
 ```

--------------------------------------------

### Observações

Na raiz do repositório há uma pasta "Relatórios" contendo arquivos 
```relatorio de Bugs.pdf,```
informando as inconsistências.  

###### Autor: 
```Carlos Caldas```

