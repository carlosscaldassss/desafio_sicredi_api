package br.com.automacao_api_sicredi.simulacoes;

import br.com.automacao_api_sicredi.baseteste.BaseTeste;
import br.com.automacao_api_sicredi.dto.SimulacaoDTO;
import br.com.automacao_api_sicredi.service.SimulacaoService;
import com.github.javafaker.Faker;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.Test;



@Feature("Testes de exclusão de Cadastros")
public class DeletarUmaSimulacao extends BaseTeste{
    @Test
    @Description("Deletar um cadastro informando o CPF")
    public void deletarUmCadastroComSucesso() {
        Faker faker = new Faker();
        String cpf = faker.number().digits(11);
        for (int i = 1; i <= 2; i++) {
            SimulacaoDTO simulacao = SimulacaoDTO.builder()
                    .nome(faker.name().name())
                    .cpf(cpf)
                    .email("teste@gmail.com")
                    .valor(5000)
                    .parcelas(3)
                    .seguro(false)
                    .build();
            if(i==1){
                SimulacaoService.cadastrarSimulacao(simulacao, 201);
            } else {
                SimulacaoService.deletarSimulacaoComCPF(cpf, 200);
            };
        };
    };

    @Test
    @Description("Tenta deletar um cadastro de um cpf que não existe no registro")
    public void deletarUmCadastroQueNaoExiste() {
        Faker faker = new Faker();

        for (int i = 1; i <= 3; i++) {
            String cpf = faker.number().digits(11);
            SimulacaoDTO simulacao = SimulacaoDTO.builder()
                    .nome(faker.name().name())
                    .cpf(cpf)
                    .email("teste@gmail.com")
                    .valor(5000)
                    .parcelas(3)
                    .seguro(false)
                    .build();
            if(i==1){
                SimulacaoService.cadastrarSimulacao(simulacao, 201);
            } else if(i==2) {
                SimulacaoService.deletarSimulacaoComCPF(cpf , 404); //BUG DEVERIA RETORNAR UM 404
            }
        };
    };
};
