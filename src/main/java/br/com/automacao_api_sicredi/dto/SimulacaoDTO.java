package br.com.automacao_api_sicredi.dto;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SimulacaoDTO {
    private int id;
    private String nome;
    private String cpf;
    private String email;
    private float valor;
    private int parcelas;
    private boolean seguro;
};
