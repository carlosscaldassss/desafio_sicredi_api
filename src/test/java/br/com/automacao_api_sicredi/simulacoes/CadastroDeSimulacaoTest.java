package br.com.automacao_api_sicredi.simulacoes;
import br.com.automacao_api_sicredi.baseteste.BaseTeste;
import br.com.automacao_api_sicredi.dto.SimulacaoDTO;
import br.com.automacao_api_sicredi.service.SimulacaoService;
import com.github.javafaker.Faker;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.Test;

@Feature("Testes de Cadastro de Simulações")
public class CadastroDeSimulacaoTest extends BaseTeste{
    Faker faker = new Faker();

    @Test
    @Description("Cadastrar uma Simulação")
    public void realizarUmNovoCadastroComSucesso() {
        SimulacaoDTO simulacao = SimulacaoDTO.builder()
                .nome(faker.name().name())
                .cpf(faker.number().digits(11))
                .email("teste@gmail.com")
                .valor(5000)
                .parcelas(3)
                .seguro(false)
                .build();
        SimulacaoService.cadastrarSimulacao(simulacao, 201);
    };

    @Test
    @Description("Cadastrar uma Simulação Com valor menor que 1000")
    public void realizarUmNovoCadastroComValorMenorQue1000() {
        SimulacaoDTO simulacao = SimulacaoDTO.builder()
                .nome(faker.name().name())
                .cpf(faker.number().digits(11))
                .email("teste@gmail.com")
                .valor(950)
                .parcelas(3)
                .seguro(false)
                .build();
        SimulacaoService.cadastrarSimulacao(simulacao, 400);
    };

    @Test
    @Description("Cadastrar uma Simulação Com Numero De Parcelar Maior que 48")
    public void realizarUmNovoCadastroComNumeroDeParcelasMaiorQue48() {
        SimulacaoDTO simulacao = SimulacaoDTO.builder()
                .nome(faker.name().name())
                .cpf(faker.number().digits(11))
                .email("teste@gmail.com")
                .valor(3000)
                .parcelas(50)
                .seguro(false)
                .build();
        SimulacaoService.cadastrarSimulacao(simulacao, 400);
    };

    @Test
    @Description("Enviar um novo cadastro sem informar o Nome")
    public void realizarUmNovoCadastroSemInformarONome(){
        SimulacaoDTO simulacao = SimulacaoDTO.builder()
                .cpf(faker.number().digits(11))
                .email("teste@gmail.com")
                .valor(5000)
                .parcelas(3)
                .seguro(false)
                .build();
        SimulacaoService.cadastrarSimulacao(simulacao, 400);
    };

    @Test
    @Description("Enviar um novo cadastro sem informar o Email")
    public void realizarUmNovoCadastroSemInformarOEmail(){
        SimulacaoDTO simulacao = SimulacaoDTO.builder()
                .nome(faker.name().name())
                .cpf(faker.number().digits(11))
                .valor(5000)
                .parcelas(3)
                .seguro(false)
                .build();
        SimulacaoService.cadastrarSimulacao(simulacao, 400);
    };

    @Test
    @Description("Enviar um novo cadastro sem informar o Valor") //BUG -- DEIXOU CADASTRAR SEMV INFORMAR O VALOR
    public void realizarUmNovoCadastroSemInformarOValor(){
        SimulacaoDTO simulacao = SimulacaoDTO.builder()
                .nome(faker.name().name())
                .cpf(faker.number().digits(11))
                .email("teste@gmail.com")
                .parcelas(3)
                .seguro(false)
                .build();
        SimulacaoService.cadastrarSimulacao(simulacao, 400);
    };

    @Test
    @Description("Enviar um novo cadastro sem informar a quantidade de Parcelas")
    public void realizarUmNovoCadastroSemInformarQuantidadeDeParcelas(){
        SimulacaoDTO simulacao = SimulacaoDTO.builder()
                .nome(faker.name().name())
                .cpf(faker.number().digits(11))
                .email("teste@gmail.com")
                .valor(3000)
                .seguro(false)
                .build();
        SimulacaoService.cadastrarSimulacao(simulacao, 400);
    };

    @Test
    @Description("Cadastrar a mesma simulação 2 vez para validar o bloqueio para cadastrar CPF duplicado")
    public void realizarUmCadastroComCpfDuplicado() {
        for (int i = 1; i <= 2; i++) {
            SimulacaoDTO simulacao = SimulacaoDTO.builder()
                    .nome(faker.name().name())
                    .cpf("12345678912")
                    .email("teste@gmail.com")
                    .valor(5000)
                    .parcelas(3)
                    .seguro(false)
                    .build();
            if(i==1){
                SimulacaoService.cadastrarSimulacao(simulacao, 201);
            } else if (i==2) {
                SimulacaoService.cadastrarSimulacao(simulacao, 400);
            } else{
                System.out.println("nao entrou");
            };
        };
    };

};
