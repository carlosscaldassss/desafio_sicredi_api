package br.com.automacao_api_sicredi.simulacoes;

import br.com.automacao_api_sicredi.baseteste.BaseTeste;
import br.com.automacao_api_sicredi.dto.OutSimulacaoDTO;
import br.com.automacao_api_sicredi.dto.SimulacaoDTO;
import br.com.automacao_api_sicredi.service.SimulacaoService;
import com.github.javafaker.Faker;
import io.qameta.allure.Feature;
import org.junit.Assert;
import io.qameta.allure.Description;
import org.junit.Test;




@Feature("Testes de Edição de Cadastros")
public class EditarUmaSimulacao extends BaseTeste {

    @Test
    @Description("Alterar o Nome em um cadastro informando o CPF")
    public void editarONomeEmUmCadastroComSucesso() {
        Faker faker = new Faker();
        String cpf = faker.number().digits(11);
        SimulacaoDTO simulacao = SimulacaoDTO.builder()
                .nome(faker.name().name())
                .cpf(cpf)
                .email("teste@gmail.com")
                .valor(5000)
                .parcelas(3)
                .seguro(false)
                .build();
        OutSimulacaoDTO retorno = null;
        for (int i = 1; i <= 2; i++) {
            if (i == 1) {
                retorno = SimulacaoService.cadastrarSimulacao(simulacao, 201);
            } else {
                simulacao.setId(retorno.getId());
                simulacao.setNome("Nome alterado");
                OutSimulacaoDTO retornoEdit = SimulacaoService.editarSimulacaoComCPF(cpf, simulacao, 200);
                Assert.assertEquals("Nome alterado", retornoEdit.getNome());
            };
        };
    };

    @Test
    @Description("Alterar o cpf em um cadastro informando o CPF")
    public void editarOCPFEmUmCadastroComSucesso() {
        Faker faker = new Faker();
        String cpf = faker.number().digits(11);
        SimulacaoDTO simulacao = SimulacaoDTO.builder()
                .nome(faker.name().name())
                .cpf(cpf)
                .email("teste@gmail.com")
                .valor(5000)
                .parcelas(3)
                .seguro(false)
                .build();
        OutSimulacaoDTO retorno = null;
        for (int i = 1; i <= 2; i++) {
            if (i == 1) {
                retorno = SimulacaoService.cadastrarSimulacao(simulacao, 201);
            } else {
                simulacao.setId(retorno.getId());
                simulacao.setCpf(cpf+"123");
                OutSimulacaoDTO retornoEdit = SimulacaoService.editarSimulacaoComCPF(cpf, simulacao, 200);
                Assert.assertEquals(cpf+"123", retornoEdit.getCpf());
            };
        };
    };

    @Test
    @Description("Alterar o email em um cadastro informando o CPF")
    public void editarOEmailEmUmCadastroComSucesso() {
        Faker faker = new Faker();
        String cpf = faker.number().digits(11);
        SimulacaoDTO simulacao = SimulacaoDTO.builder()
                .nome(faker.name().name())
                .cpf(cpf)
                .email("teste@gmail.com")
                .valor(5000)
                .parcelas(3)
                .seguro(false)
                .build();
        OutSimulacaoDTO retorno = null;
        for (int i = 1; i <= 2; i++) {
            if (i == 1) {
                retorno = SimulacaoService.cadastrarSimulacao(simulacao, 201);
            } else {
                simulacao.setId(retorno.getId());
                simulacao.setEmail("emailEditado@gmail.com");
                OutSimulacaoDTO retornoEdit = SimulacaoService.editarSimulacaoComCPF(cpf, simulacao, 200);
                Assert.assertEquals("emailEditado@gmail.com", retornoEdit.getEmail());
            };
        };
    };

    @Test
    @Description("Alterar o valor em um cadastro informando o CPF")
    public void editarOValorEmUmCadastroComSucesso() {
        Faker faker = new Faker();
        String cpf = faker.number().digits(11);
        SimulacaoDTO simulacao = SimulacaoDTO.builder()
                .nome(faker.name().name())
                .cpf(cpf)
                .email("teste@gmail.com")
                .valor(5000)
                .parcelas(3)
                .seguro(false)
                .build();
        OutSimulacaoDTO retorno = null;
        for (int i = 1; i <= 2; i++) {
            if (i == 1) {
                retorno = SimulacaoService.cadastrarSimulacao(simulacao, 201);
            } else {
                simulacao.setId(retorno.getId());
                simulacao.setValor(2000);
                OutSimulacaoDTO retornoEdit = SimulacaoService.editarSimulacaoComCPF(cpf, simulacao, 200);
                Assert.assertEquals(2000, retornoEdit.getValor(), 2); //BUG, O VALRO NÃO ESTA SENDO ALTERADO
            };
        };
    };

    @Test
    @Description("Alterar a quantidade de parcelas em um cadastro informando o CPF")
    public void editarQuantidadeDePArcelasEmUmCadastroComSucesso() {
        Faker faker = new Faker();
        String cpf = faker.number().digits(11);
        SimulacaoDTO simulacao = SimulacaoDTO.builder()
                .nome(faker.name().name())
                .cpf(cpf)
                .email("teste@gmail.com")
                .valor(5000)
                .parcelas(3)
                .seguro(false)
                .build();
        OutSimulacaoDTO retorno = null;
        for (int i = 1; i <= 2; i++) {
            if (i == 1) {
                retorno = SimulacaoService.cadastrarSimulacao(simulacao, 201);
            } else {
                simulacao.setId(retorno.getId());
                simulacao.setParcelas(7);
                OutSimulacaoDTO retornoEdit = SimulacaoService.editarSimulacaoComCPF(cpf, simulacao, 200);
                Assert.assertEquals(7, retornoEdit.getParcelas(), 2);
            };
        };
    };

    @Test
    @Description("Alterar opção Seguro em um cadastro informando o CPF")
    public void editarSeguroEmUmCadastroComSucesso() {
        Faker faker = new Faker();
        String cpf = faker.number().digits(11);
        SimulacaoDTO simulacao = SimulacaoDTO.builder()
                .nome(faker.name().name())
                .cpf(cpf)
                .email("teste@gmail.com")
                .valor(5000)
                .parcelas(3)
                .seguro(false)
                .build();
        OutSimulacaoDTO retorno = null;
        for (int i = 1; i <= 2; i++) {
            if (i == 1) {
                retorno = SimulacaoService.cadastrarSimulacao(simulacao, 201);
            } else {
                simulacao.setId(retorno.getId());
                simulacao.setSeguro(true);
                OutSimulacaoDTO retornoEdit = SimulacaoService.editarSimulacaoComCPF(cpf, simulacao, 200);
                Assert.assertEquals(true, retornoEdit.getSeguro());

            };
        };
    };

    @Test
    @Description("Alterar um cadastro informando um CPF que não esta cadastrado")
    public void editarUmCadastroSeminformarOCPF() {
        Faker faker = new Faker();
        String cpf = faker.number().digits(11);
        SimulacaoDTO simulacao = SimulacaoDTO.builder()
                .nome(faker.name().name())
                .cpf(cpf)
                .email("teste@gmail.com")
                .valor(5000)
                .parcelas(3)
                .seguro(false)
                .build();
        OutSimulacaoDTO retorno = null;
        for (int i = 1; i <= 2; i++) {
            if (i == 1) {
                retorno = SimulacaoService.cadastrarSimulacao(simulacao, 201);
            } else {
                cpf = "00000000000";
                simulacao.setId(retorno.getId());
                simulacao.setNome("edit");
                OutSimulacaoDTO retornoEdit = SimulacaoService.editarSimulacaoComCPF(cpf, simulacao, 404);

            };
        };
    };
};
