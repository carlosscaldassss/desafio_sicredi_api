package br.com.automacao_api_sicredi.service;

import br.com.automacao_api_sicredi.baseteste.BaseTeste;
import br.com.automacao_api_sicredi.dto.MensagemDTO;
import io.restassured.response.ValidatableResponse;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class RestricaoService extends BaseTeste {

    public static MensagemDTO consultarCpfComRestricao(String cpf, int statusCode) {
        MensagemDTO resposta = given()
                .spec(reqSpec)
                .baseUri(baseURI)
                .when()
                .get("v1/restricoes/" + cpf )
                .then()
                .log().all()
                .statusCode(statusCode)
                .spec(resSpec)
                .extract().as(MensagemDTO.class);
        return resposta;
    }


    public static ValidatableResponse consultarCpfSemRestricao(String cpf, int statusCod) {
        return given()
                .pathParam("cpf", cpf)
                .spec(reqSpec)
                .baseUri(baseURI)
                .when()
                .get("v1/restricoes/{cpf}")
                .then()
                .statusCode(statusCod);
    }
}
