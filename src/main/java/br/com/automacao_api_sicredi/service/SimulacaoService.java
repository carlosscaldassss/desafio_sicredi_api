package br.com.automacao_api_sicredi.service;

import br.com.automacao_api_sicredi.baseteste.BaseTeste;
import br.com.automacao_api_sicredi.dto.MensagemDTO;
import br.com.automacao_api_sicredi.dto.OutSimulacaoDTO;
import br.com.automacao_api_sicredi.dto.SimulacaoDTO;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.is;

public class SimulacaoService extends BaseTeste {
   public static OutSimulacaoDTO[] consultarTodasSimulacoes(int statusCode){
      OutSimulacaoDTO[] resposta = given()
              .contentType(ContentType.JSON)
              .spec(reqSpec)
              .baseUri(baseURI)
              .when()
              .get("v1/simulacoes/")
              .then()
              .statusCode(statusCode)
              .spec(resSpec)
              .extract().as(OutSimulacaoDTO[].class);
      return resposta;
   };

   public static OutSimulacaoDTO consultarSimulacaoComCPF(String cpf, int statusCode) {
      OutSimulacaoDTO resposta = given()
              .spec(reqSpec)
              .baseUri(baseURI)
              .when()
              .get("v1/simulacoes/"+cpf)
              .then()
              .log().all()
              .statusCode(statusCode)
              .spec(resSpec)
              .extract().as(OutSimulacaoDTO.class);
      return resposta;
   };

   public static MensagemDTO consultarSimulacaoCpfNaoCadastrado(String cpf, int statusCode) {

      MensagemDTO resposta = given()
              .spec(reqSpec)
              .baseUri(baseURI)
              .when()
              .get("/v1/simulacoes/" + cpf)
              .then()
              .statusCode(statusCode)
              .spec(resSpec)
              .extract().as(MensagemDTO.class);

      return resposta;

   };
   public static OutSimulacaoDTO cadastrarSimulacao(SimulacaoDTO simulacao, int statusCode){

      OutSimulacaoDTO resposta = given()
              .contentType(ContentType.JSON)
              .body(simulacao)
              .when()
              .post("/v1/simulacoes/")
              .then()
              .log().all()
              .statusCode(statusCode)
              .extract().as(OutSimulacaoDTO.class);

      return resposta;
   };

   public static ValidatableResponse deletarSimulacaoComCPF(String cpf, int statusCode) {

      return given()
              .pathParam("id", cpf)
              .spec(reqSpec)
              .baseUri(baseURI)
              .when()
              .delete("v1/simulacoes/{id}")
              .then()
              .log().all()
              .statusCode(statusCode)
              .body(is("OK"));
   };

   public static OutSimulacaoDTO editarSimulacaoComCPF(String cpf, SimulacaoDTO simulacao2, int statusCode) {
      OutSimulacaoDTO resposta = given()
              .pathParam("cpf", cpf)
              .spec(reqSpec)
              .baseUri(baseURI)
              .contentType(ContentType.JSON)
              .body(simulacao2)
              .when()
              .put("v1/simulacoes/{cpf}")
              .then()
              .log().all()
              .statusCode(statusCode)
              .spec(resSpec)
              .extract().as(OutSimulacaoDTO.class);

      return resposta;
   };

};
